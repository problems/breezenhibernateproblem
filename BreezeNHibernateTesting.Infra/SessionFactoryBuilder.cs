﻿using BreezeNHibernateTesting.Domain;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Ninject.Activation;
using System;

namespace BreezeNHibernateTesting.Infra
{
	public class SessionFactoryBuilder : IProvider
	{
		private ISessionFactory _sessionFactory;
		private readonly Configuration _configuration;

		/// <summary>
		/// constructor configures a SessionFactory based on the configuration passed in
		/// </summary>	
		/// <param name="configuration"></param>
		public SessionFactoryBuilder()
		{
			var mapConfig = new AutomappingConfiguration();
			_configuration = Fluently.Configure(new Configuration().Configure())
					.Mappings(m => m.AutoMappings.Add(AutoMap.AssemblyOf<IEntidade>(mapConfig)))
					.ExposeConfiguration(SetuptDatabase)
					.BuildConfiguration();
			_sessionFactory = _configuration.BuildSessionFactory();
		}

		private static void SetuptDatabase(Configuration config)
		{
			var schema = new SchemaExport(config);
			//schema.Execute(true, true, false);
			//schema.Drop(true, true);
			//schema.Create(false, false);
		}

		#region IProvider Members

		public object Create(IContext context)
		{
			return _sessionFactory;
		}

		public Type Type
		{
			get { return typeof(ISessionFactory); }
		}

		#endregion
	}
}
