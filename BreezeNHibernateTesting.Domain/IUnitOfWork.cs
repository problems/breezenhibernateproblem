﻿using System;

namespace BreezeNHibernateTesting.Domain
{
	public interface IUnitOfWork : IDisposable
	{
		void Commit();
		void Rollback();
	}

	public interface IUnitOfWork<TContext> : IUnitOfWork
	{
		TContext Context { get; }
	}
}
