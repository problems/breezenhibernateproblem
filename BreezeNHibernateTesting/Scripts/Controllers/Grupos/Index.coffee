﻿ko.bindingHandlers.formSearch =
	init: (element, valueAccessor, allBindings) ->
		collection = valueAccessor()
		urlFilter = allBindings().urlFilterBuild

		ko.applyBindingsToNode element,
			submit: (frm) -> 
				url= urlFilter
				url = urlFilter() if $.isFunction(urlFilter)
				url  = "?$filter=" + url if url
				$.getJSON $(frm).attr("action") + url, (data) ->
					collection.removeAll()
					collection data.value
					

vm = 
	Grupos: ko.observableArray []
	# Pesquisa
	nome: ko.observable()
	id: ko.observable()
	urlFilter: -> 
		nomeSearch  = ko.unwrap vm.nome
		idSearch  = ko.unwrap vm.id
	
		filters = []

		filters.push "substringof('#{nomeSearch}', Nome)" if nomeSearch
		filters.push "Id eq #{idSearch}" if idSearch
		if filters.length > 0 then filters.join " or " else  ""


ko.applyBindings vm
