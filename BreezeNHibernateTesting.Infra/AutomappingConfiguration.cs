﻿using BreezeNHibernateTesting.Domain;
using FluentNHibernate.Automapping;
using System;
using System.Linq;

namespace BreezeNHibernateTesting.Infra
{
	public class AutomappingConfiguration : DefaultAutomappingConfiguration
	{
		public override bool ShouldMap(Type type)
		{
			return type.GetInterfaces().Any(y => y == typeof(IEntidade));
		}
	}
}
