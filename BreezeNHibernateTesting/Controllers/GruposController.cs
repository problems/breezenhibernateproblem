﻿using BreezeNHibernateTesting.Domain;
using System.Web.Mvc;

namespace BreezeNHibernateTesting.Controllers
{
    public class GruposController : Controller
    {
		private readonly IGruposRepositorio _gruposRepositorio;
		public GruposController(IGruposRepositorio gruposRepositorio)
		{
			_gruposRepositorio = gruposRepositorio;
		}

        public ActionResult Index()
        {
			return View();
        }

		public ActionResult Novo()
		{
			return View(new Grupo());
		}

		[HttpPost]
		public ActionResult Novo(Grupo vm)
		{
			return View();
		}
	}
}