﻿using BreezeNHibernateTesting.Domain;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace BreezeNHibernateTesting.Infra
{
	public class Repositorio<T> : IRepositorio<T>
			where T : class, IEntidade
	{

		private readonly NHUnitOfWork _unitOfWork;
		public IUnitOfWork UnitOfWork
		{
			get { return _unitOfWork; }
		}
		private readonly ISession _session;

		public Repositorio(IUnitOfWork unitOfWork)
		{
			_unitOfWork = (NHUnitOfWork)unitOfWork;
			_session = _unitOfWork.Context.SessionFactory.GetCurrentSession();
		}

		public void Remover(T obj)
		{
			_session.Delete(obj);
		}

		public void Armazenar(T obj)
		{
			_session.SaveOrUpdate(obj);
		}

		public IQueryable<T> All()
		{
			return _session.Query<T>();
		}

		public object Get(Type entity, int id)
		{
			return _session.Get(entity, id);
		}

		public T Get(Expression<Func<T, bool>> expression)
		{
			return Query(expression).SingleOrDefault();
		}

		public T Get(int id)
		{
			return _session.Get<T>(id);
		}

		public IQueryable<T> Query(Expression<Func<T, bool>> expression)
		{
			return All().Where(expression);
		}
	}
}
