﻿using BreezeNHibernateTesting.Domain;
using NHibernate;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using Ninject.Web.Common;

namespace BreezeNHibernateTesting.Infra
{
	public class RepositoriosModule : NinjectModule
	{
		public override void Load()
		{
			// NHibernate 
			Bind<ISessionFactory>().ToProvider<SessionFactoryBuilder>().InSingletonScope();
			Bind<ISession>().ToMethod(CreateSession).InRequestScope();

			Bind<IUnitOfWork>().To<NHUnitOfWork>().InRequestScope();

			//Model Repositories
			Bind<IRepositorio<Grupo>, IGruposRepositorio>().To<GruposRepositorio>().InRequestScope();
		}

		private ISession CreateSession(IContext context)
		{
			return context.Kernel.Get<ISessionFactory>().OpenSession();
		}
	}
}
