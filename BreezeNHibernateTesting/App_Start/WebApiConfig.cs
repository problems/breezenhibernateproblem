﻿using BreezeNHibernateTesting.Domain;
using System.Web.Http;
using System.Web.Http.OData.Builder;

namespace BreezeNHibernateTesting
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
			// OData
			config.Routes.MapODataRoute(routeName: "OData", routePrefix: "odata", model: CreateModelBuider(config).GetEdmModel());
			config.EnableQuerySupport();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

		public static ODataModelBuilder CreateModelBuider(HttpConfiguration config)
		{
			var modelBuilder = new ODataConventionModelBuilder(config);

			var grupo = modelBuilder.EntitySet<Grupo>("Grupos");
			grupo.EntityType.HasKey(p => p.Id);

			modelBuilder.Namespace = "BreezeNHibernateTesting.Domain";
			return modelBuilder;
		}
    }
}