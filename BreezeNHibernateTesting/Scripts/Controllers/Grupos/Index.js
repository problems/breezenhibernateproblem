﻿(function() {
  var vm;



  ko.bindingHandlers.formSearch = {
    init: function(element, valueAccessor, allBindings) {
      var collection, urlFilter;
      collection = valueAccessor();
      urlFilter = allBindings().urlFilterBuild;
      return ko.applyBindingsToNode(element, {
        submit: function(frm) {
          var url;
          url = urlFilter;
          if ($.isFunction(urlFilter)) {
            url = urlFilter();
          }
          if (url) {
            url = "?$filter=" + url;
          }
          return $.getJSON($(frm).attr("action") + url, function(data) {
            collection.removeAll();
            return collection(data.value);
          });
        }
      });
    }
  };

  vm = {
    Grupos: ko.observableArray([]),
    nome: ko.observable(),
    id: ko.observable(),
    urlFilter: function() {
      var filters, idSearch, nomeSearch;
      nomeSearch = ko.unwrap(vm.nome);
      idSearch = ko.unwrap(vm.id);
      filters = [];
      if (nomeSearch) {
        filters.push("substringof('" + nomeSearch + "', Nome)");
      }
      if (idSearch) {
        filters.push("Id eq " + idSearch);
      }
      if (filters.length > 0) {
        return filters.join(" or ");
      } else {
        return "";
      }
    }
  };

  ko.applyBindings(vm);

}).call(this);
