﻿using BreezeNHibernateTesting.Domain;
using System.Linq;
using System.Web.Http.OData;

namespace BreezeNHibernateTesting.Api
{
	public class GruposController : EntitySetController<Grupo, int>
	{
		private readonly IGruposRepositorio _gruposRepositorio;
		public GruposController(IGruposRepositorio gruposRepositorio)
		{
			_gruposRepositorio = gruposRepositorio;
		}

		public override IQueryable<Grupo> Get()
		{
			return _gruposRepositorio.All();
		}
	}
}
