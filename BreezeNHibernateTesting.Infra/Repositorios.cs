﻿using BreezeNHibernateTesting.Domain;

namespace BreezeNHibernateTesting.Infra
{
	public class GruposRepositorio : Repositorio<Grupo>, IGruposRepositorio
	{
		public GruposRepositorio(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{ }
	}
}
