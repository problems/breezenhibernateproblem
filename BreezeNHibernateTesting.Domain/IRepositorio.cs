﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace BreezeNHibernateTesting.Domain
{
	public interface IRepositorio<T>
		where T : class, IEntidade
	{
		IUnitOfWork UnitOfWork { get; }

		T Get(int id);
		object Get(Type entity, int id);

		void Armazenar(T entity);

		IQueryable<T> All();
		T Get(Expression<System.Func<T, bool>> expression);
		IQueryable<T> Query(Expression<System.Func<T, bool>> expression);
	}
}
