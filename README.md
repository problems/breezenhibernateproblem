## Breeze  and NHibernate problems
-----------------------------------
This project aims to simulate problems with [NHibernate](https://github.com/nhibernate/nhibernate-core) and [Breeze](https://github.com/IdeaBlade/Breeze) and possible solutions if possible.

### Problem

Using Breeze in a webapi 2 and NHibernate project.

With the aim of using OData commands like `$expand` and `$substringof`, we decided to opt for the framework Breeze. But the version with support for NHibernate is still in beta and we not found code examples of how to use this framework.

This is a well close to a real application sample project (using standards like UnitOfWork) simulating the problem.

### Environment

#### Database

The application uses localdb (instance Problems) to host the database. 
Before starting, follow the steps below:

1. Create an instance in localdb called Problems (if not created) `sqllocaldb c Problems`;
2. Start in the instance `sqllocaldb s Problems`;
3. With SQL Manager Studio, connect into instance `(localdb)\Problems`;
4. Create or attach the database `breezenhibernatetesting` into instance.
5. Create same records in `Grupos` table.


### Published Forums

 1. [How to implement Breeze with NHibernate and webapi 2](http://stackoverflow.com/questions/20681180/how-to-implement-breeze-with-nhibernate-and-webapi-2)
 2. [Breeze.Server.ContextProvider.NH not exist](http://stackoverflow.com/questions/20565767/breeze-server-contextprovider-nh-not-exist)
