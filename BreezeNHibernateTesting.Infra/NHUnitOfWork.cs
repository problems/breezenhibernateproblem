﻿using BreezeNHibernateTesting.Domain;
using NHibernate;
using System;
using System.Data;
using NHContext = global::NHibernate.Context;

namespace BreezeNHibernateTesting.Infra
{
	public class NHUnitOfWork : IUnitOfWork<ISession>
	{
		private readonly ITransaction _transaction;
		private readonly ISessionFactory _sessionFactory;

		public NHUnitOfWork(ISessionFactory sessionFactory)
		{
			_sessionFactory = sessionFactory;
			Context = _sessionFactory.OpenSession(); //StaticSessionManager.OpenSession();
			NHContext.CurrentSessionContext.Bind(Context);

			_transaction = Context.BeginTransaction(IsolationLevel.ReadCommitted);
		}

		public ISession Context { get; private set; }

		public void Commit()
		{
			if (!_transaction.IsActive)
				throw new InvalidOperationException("Oops! We don't have an active transaction");
			_transaction.Commit();
		}

		public void Rollback()
		{
			if (_transaction.IsActive)
				_transaction.Rollback();
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				var currentSession = NHContext.CurrentSessionContext.Unbind(_sessionFactory);
				if (currentSession != null && currentSession.IsOpen)
					currentSession.Close();
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
